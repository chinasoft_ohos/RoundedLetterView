package com.github.pavlospt.demo;

import com.github.pavlospt.roundedletter_view.PxUtil;
import org.junit.Assert;
import org.junit.Test;

public class ExampleTest {

    @Test
    public void  formatNum() {
        String s = PxUtil.formatNum(10);
        Assert.assertNotNull(s);
    }

    @Test
    public void  floatNum() {
        String s = PxUtil.formatNum(10.1f);
        Assert.assertNotNull(s);
    }
}
