package com.github.pavlospt.demo;

import com.github.pavlospt.roundedletter_view.RoundedLetterView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

import java.util.List;

public class SampleItemProvider extends RecycleItemProvider {
    private List<SampleItem> list;
    private AbilitySlice slice;
    public SampleItemProvider(List<SampleItem> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }
    @Override
    public int getCount() {
        return list.size();
    }
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_sample, null, false);
        }else {
            cpt = convertComponent;
        }
        SampleItem sampleItem = list.get(position);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_index);
        RoundedLetterView letterView = (RoundedLetterView) cpt.findComponentById(ResourceTable.Id_round_view);
        letterView.setTitleText(sampleItem.getName());
        letterView.setBackgroundColor(Color.getIntColor(sampleItem.getColor()));
        text.setText(sampleItem.getValue());
        return cpt;
    }
}
