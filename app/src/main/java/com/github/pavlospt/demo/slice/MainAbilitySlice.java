package com.github.pavlospt.demo.slice;

import com.github.pavlospt.demo.ResourceTable;
import com.github.pavlospt.demo.SampleItem;
import com.github.pavlospt.demo.SampleItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

   private ListContainer listContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initListContainer();

    }

    private void initListContainer() {
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        List<SampleItem> list = getData();
        SampleItemProvider sampleItemProvider = new SampleItemProvider(list,this);
        listContainer.setItemProvider(sampleItemProvider);

        listContainer.setItemClickedListener((listContainer, component, position, id) -> {
            SampleItem item = (SampleItem) listContainer.getItemProvider().getItem(position);
            new ToastDialog(getContext())
                    .setText("clicked:"+item.getName())
                    // Toast显示在界面中间
                    .setAlignment(LayoutAlignment.CENTER)
                    .show();
        });

        //在开启回弹效果后，可以调用setReboundEffectParams()方法调整回弹效果。
        listContainer.setReboundEffectParams(40,0.6f,20);

    }
    private ArrayList<SampleItem> getData() {
        ArrayList<SampleItem> list = new ArrayList<>();
        for (int i = 0; i <= 80; i++) {
            if (i%2==0){
                SampleItem e = new SampleItem(i + "", i + "8888888888","#00ff00");
                list.add(e);
            }else {
                SampleItem e = new SampleItem(i + "", i + "8888888888","#ff0000");
                list.add(e);
            }

        }
        return list;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
