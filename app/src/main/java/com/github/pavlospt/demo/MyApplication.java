package com.github.pavlospt.demo;

import com.github.pavlospt.roundedletter_view.PxUtil;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        PxUtil.initContext(this);
    }
}
