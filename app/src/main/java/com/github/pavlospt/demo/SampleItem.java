package com.github.pavlospt.demo;

public class SampleItem {
    private String name;
    private String value;
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SampleItem(String name,String value,String color) {
        this.name = name;
        this.value = value;
        this.color = color;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
