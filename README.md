

# RoundedLetterView


#### 项目介绍
- 项目名称：RoundedLetterView
- 所属系列：openharmony的第三方组件适配移植
- 功能：RoundedLetterView是一个仿通讯录UI显示的控件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：maven 1.3
#### 效果演示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/161112_07837b29_8950844.gif "demo.gif")

#### 安装教程


方式一：

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

    implementation('com.gitee.chinasoft_ohos:RoundedLetterView:1.1.1')

    ......  

 }

 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
```
 <com.github.pavlospt.roundedletter_view.RoundedLetterView
        ohos:id="$+id:round_view"
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:left_margin="20vp"
        ohos:layout_alignment="center"
        hap:RoundedLetterView_rlv_circle_size="60"
        />

```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.1.1
- 0.0.1-SNAPSHOT

### 版权和许可信息

```
Copyright 2014 Pavlos-Petros Tournaris

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
