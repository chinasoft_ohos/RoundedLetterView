/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package com.github.pavlospt.roundedletter_view;

import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;

/**
 * AttrUtils
 */
public class AttrUtils {
    /**
     * getStringFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return String
     */
    public static String getStringFromAttr(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        if (attrSet == null) {
            return value;
        }
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getStringValue();
        }
        return value;
    }

    /**
     * getDimensionFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Integer
     */
    public static Integer getDimensionFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet == null) {
            return value;
        }
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getDimensionValue();
        }
        return value;
    }

    /**
     * getIntegerFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Integer
     */
    public static Integer getIntegerFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet == null) {
            return value;
        }
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getIntegerValue();
        }
        return value;
    }

    /**
     * getFloatFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return float
     */
    public static float getFloatFromAttr(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        if (attrSet == null) {
            return value;
        }
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getFloatValue();
        }
        return value;
    }

    /**
     * getLongFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Long
     */

    public static Long getLongFromAttr(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
        if (attrSet == null) {
            return value;
        }
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getLongValue();
        }
        return value;
    }

    /**
     * getColorFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return int
     */
    public static int getColorFromAttr(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        if (attrSet == null) {
            return value;
        }
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getColorValue().getValue();
        }
        return value;
    }

    /**
     * getBooleanFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return boolean
     */
    public static boolean getBooleanFromAttr(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        if (attrSet == null) {
            return value;
        }
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getBoolValue();
        }
        return value;
    }

    /**
     * getColor
     *
     * @param color color
     * @return Color
     */
    public static Color getColor(int color) {
        return new Color(color);
    }
}
