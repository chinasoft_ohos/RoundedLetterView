package com.github.pavlospt.roundedletter_view;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

public class RoundedLetterView extends Component implements Component.DrawTask {

    private static final int DEFAULT_VIEW_SIZE = 50;
    private static final int DEFAULT_TITLE_SIZE = 15;
    private static final String DEFAULT_TITLE = "A";

    private int mTitleColor;
    private int mBackgroundColor;
    private String mTitleText = DEFAULT_TITLE;
    private int mTitleSize = DEFAULT_TITLE_SIZE;

    private Paint mTitleTextPaint;
    private Paint mBackgroundPaint;
    private RectFloat mInnerRectF;
    private int mViewSize;

    //    private Typeface mFont = Typeface.defaultFromStyle(Typeface.NORMAL);
    private Font mFont = Font.DEFAULT;

    /**
     * int值  XML里不需要+vp .保持跟宽高一致  控制圆圈大小
     */
    private Integer mMircle_size;

    /**
     * RoundedLetterView
     *
     * @param context context
     */
    public RoundedLetterView(final Context context) {
        super(context);
        init(null);
        addDrawTask(this);
    }

    /**
     * RoundedLetterView
     *
     * @param context context
     * @param attrs attrs
     */
    public RoundedLetterView(final Context context, final AttrSet attrs) {
        super(context, attrs);
        init(attrs);
        addDrawTask(this);
    }

    /**
     * RoundedLetterView
     *
     * @param context context
     * @param attrs attrs
     * @param defStyle defStyle
     */
    public RoundedLetterView(final Context context, final AttrSet attrs,
                             final int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
        addDrawTask(this);
    }

    private void init(final AttrSet attrs) {

        mTitleText = AttrUtils.getStringFromAttr(attrs, "RoundedLetterView_rlv_titleText",
                "A");
        mTitleColor = AttrUtils.getColorFromAttr(attrs, "RoundedLetterView_rlv_titleColor",
                Color.getIntColor("#2A2A2A"));
        mTitleSize = AttrUtils.getDimensionFromAttr(attrs, "RoundedLetterView_rlv_titleSize",
                (int) PxUtil.fp2px(DEFAULT_TITLE_SIZE));
        mBackgroundColor = AttrUtils.getColorFromAttr(attrs, "RoundedLetterView_rlv_backgroundColorValue",
                Color.getIntColor("#FF00FFFF"));
        mMircle_size = AttrUtils.getIntegerFromAttr(attrs, "RoundedLetterView_rlv_circle_size",
                DEFAULT_VIEW_SIZE);

        //Title TextPaint
        mTitleTextPaint = new Paint();
        mTitleTextPaint.setAntiAlias(true);
        mTitleTextPaint.setFont(mFont);
        mTitleTextPaint.setTextAlign(TextAlignment.CENTER);
        mTitleTextPaint.setColor(new Color(mTitleColor));
        mTitleTextPaint.setTextSize(mTitleSize);
        //Background Paint
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setStyle(Paint.Style.FILL_STYLE);
        mBackgroundPaint.setColor(new Color(mBackgroundColor));
        mInnerRectF = new RectFloat();

        setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {

                int width = (int) PxUtil.vp2px(mMircle_size);
                int height = (int) PxUtil.vp2px(mMircle_size);

                mViewSize = Math.min(width, height);
                invalidate();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });
    }

    private void invalidateTextPaints() {
        mTitleTextPaint.setFont(mFont);
        mTitleTextPaint.setTextSize(mTitleSize);
        mTitleTextPaint.setColor(new Color(mTitleColor));
        invalidate();
    }

    private void invalidatePaints() {
        mBackgroundPaint.setColor(new Color(mBackgroundColor));
        invalidate();
    }

    /**
     * addDrawTask
     *
     * @param task task
     */
    @Override
    public void addDrawTask(DrawTask task) {
        super.addDrawTask(task);
        task.onDraw(this, mCanvasForTaskOverContent);
    }

    /**
     * onDraw
     *
     * @param component component
     * @param canvas canvas
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        mInnerRectF.fuse(0, 0, mViewSize, mViewSize);
//        mInnerRectF.offset((getWidth() - mViewSize) / 2, (getHeight() - mViewSize) / 2);
        int left = (getWidth() - mViewSize) / 2;
        mInnerRectF.left = (float) ((double)mInnerRectF.left + left);
        int top = (getHeight() - mViewSize) / 2;
        mInnerRectF.top = (float) ((double) mInnerRectF.top + top);
        float centerX = mInnerRectF.getCenter().getPointX();
        float centerY = mInnerRectF.getCenter().getPointY();

        int xPos = (int) centerX;
        int yPos = (int) ((double)centerY - ((double)mTitleTextPaint.descent() + (double)mTitleTextPaint.ascent()) / 2);

        canvas.drawOval(mInnerRectF, mBackgroundPaint);

        canvas.drawText(mTitleTextPaint, mTitleText,
                xPos,
                yPos);
    }

    /**
     * Gets the title string attribute value.
     *
     * @return The title string attribute value.
     */
    public String getTitleText() {
        return mTitleText;
    }

    /**
     * Sets the view's title string attribute value.
     *
     * @param title The example string attribute value to use.
     */
    public void setTitleText(String title) {
        mTitleText = title;
        invalidate();
    }

    /**
     * Gets the background color attribute value.
     *
     * @return The background color attribute value.
     */
    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    /**
     * Sets the view's background color attribute value.
     *
     * @param backgroundColor The background color attribute value to use.
     */
    public void setBackgroundColor(int backgroundColor) {
        mBackgroundColor = backgroundColor;
        invalidatePaints();
    }

    /**
     * Gets the title size dimension attribute value.
     *
     * @return The title size dimension attribute value.
     */
    public float getTitleSize() {
        return mTitleSize;
    }

    /**
     * Sets the view's title size dimension attribute value.
     *
     * @param titleSize The title size dimension attribute value to use.
     */
    public void setTitleSize(final int titleSize) {
        mTitleSize = titleSize;
        invalidateTextPaints();
    }

    /**
     * Sets the view's title typeface.
     *
     * @param font The typeface to be used for the text.
     */
    public void setTextTypeface(final Font font) {
        this.mFont = font;
        invalidateTextPaints();
    }

    /**
     * Sets the view's title color attribute value.
     *
     * @param titleColor The title color attribute value to use.
     */
    public void setTitleColor(final int titleColor) {
        mTitleColor = titleColor;
        invalidateTextPaints();
    }
}
